# **Orbit Simulator** #

This program simulates planetary orbits around a given star, presenting it in a 3D real-time animation. The default system is the Solar System, where the Sun is the central star, and all the eight planets are orbiting it. One can, however, change the planets characteristics, as well as the star's, and add or remove planets at will, in order to check the behavior of a given system.

It uses 2-Body calculations of Gravitational Forces to obtain the correct velocity vector of each planet, as well as acceleration. To avoid numerical errors on long periods of execution, the algorithm solves the said ODE through the Runge-Kutta 4th Order iterative method. This allows precise elliptical orbits with basically no error to be calculated.

## **What it considers** ##

The Orbit Simulator **considers** planets and a single star, in the center of the system. It is possible to modify characteristics such as mass, radius, translational velocity and rotational speed of a body, including the star. It allows the insertion and removal of planets.

## **What it does not consider yet** ##

The simulator **does not** consider natural satellites, such as the Moon, or comets. Altough, one could, if entering the correct informations, simulate a comet or satellite as a small planet.

## **Running** ##

The Orbit Simulator is programmed in Python 2.7. The 3D animation is done using the VPython package ([link](http://vpython.org/)), which only runs under Windows on its latest versions, or under Linux platforms by means of Wine program. Besides Python 2.7, and VPython, nothing else is needed to run the simulator.

## **About** ##

Done for the INF01121 - Programming Language Models, 2014/2, at UFRGS.


Install:
	1) Python Visual:
		sudo apt-get install python-visual
	2) WxPython:
		sudo apt-get install python-wxgtk3.0