class GenList:
	'''
	Uma lista simplificada de tipo generico
	'''
	__item = None
	__next = None
	
	def __init__(self):
		self.__item = None
		self.__next = None
	
	def setItem(self, item):
		self.__item = item
	
	def getItem(self):
		return self.__item
	
	def __getNext(self):
		return self.__next
	
	def __setNext(self, item):
		self.__next = item
	
	def append(self, newItem):
		it = self
		aux = None
		
		while it!=None:
			aux = it
			it = it.__getNext()
		
		if aux == None:
			newN = GenList()
			newN.setItem(newItem)
			self.__setNext(newN)
		else:
			newN = GenList()
			newN.setItem(newItem)
			aux.__setNext(newN)
	
	
	def getItemAt(self, index):
		it = self
		i=0
		while i<index and it != None:
			it=it.__getNext()
			i+=1
		
		if it == None:
			return None
		else:
			return it.getItem()
	
	def __del__(self):
		if self.__next == None:
			del self
			return
		
		self.__next.__del__()
		del self
	