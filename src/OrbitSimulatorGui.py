from visual import *
from visual.graph import *
import wx
from Planet import *
from OrbitSimulator import *
from GenList import *

#TODO:
#       1) Delete completely body from animation
#       2) Check body addition
#       


class OrbitSimulatorGui():
        # An orbit simulator GUI with all its components and interactions

        # A simple window to contain all the GUI
        __window = None
        # An static box to hold the simulation displayer
        __sim_box = None
        # A 3D display of the simulation
        __disp = None
        
        # 
        __addDownPlanetBtn = None
        __posXaddArea = None                    #Caixa de entrada da posicao X de um planeta
        __posZaddArea = None                    #Caixa de entrada da posicao Z de um planeta
        __massAddArea = None                    #Caixa de entrada da massa de um planeta
        __radiusAddArea = None                  #Caixa de entrada do raio de um planeta
        __planetColorCombo = None               #Combo com lista de cores para os planetas
        __transVXaddArea = None                 #Caixa de entrada da velocidade de translacao em X de um planeta
        __transVZaddArea = None                 #Caixa de entrada da velocidade de translacao em Z de um planeta
        __rotSaddArea = None                    #Caixa de entrada da velocidade de rotacao de um planeta
        __nameAddArea = None                    #Caixa de entrada do nome de um planeta
        
        __nameAddLabel = None                   #Label da caixa de nome
        __posXaddLabel = None                   #Label da caixa de entrada X
        __posZaddLabel = None                   #Label da caixa de entrada Z
        __massAddLabel = None                   #Label da caixa de entrada da Massa
        __radiusAddLabel = None                 #Label da caixa de entrada do Raio
        __transVXaddLabel = None                #Label da caixa de entrada da velocidade X
        __transVZaddLabel = None                #Label da caixa de entrada da velocidade Z
        __rotSaddLabel = None                   #Label da caixa de entrada da velocidade de rotacao
        __colorComboLabel = None                #Label do combo de cores
        
        __planetAddBtn = None                   #Botao de adicao de planetas
        __addDownPlanetBtn = None               #Botao que mostra as opcoes para adicao de planetas (inicialmente escondidas)
        
        __adding_body = False                  #Flag indicando se planetas estao sendo adicionados
        
        
        # UI de controles da simulacao
        __pause_btn = None                               #Botao de pausa
        __rb_names = None                                #Botoes de radio para mostrar/esconder nomes de planetas
        __rb_orbits = None                               #Botoes de radio para mostrar/esconder orbitas de planetas
        __rate_slider = None                             #slider para escolher velocidade da animacao
        __control_box = None                             #Caixa estatica para os controles
        __PAUSE_TEXT = 'Pause'
        __CONTINUE_TEXT = 'Continue'
        __ARATE_SPEED_TEXT = 'Animation Speed:'
        
        # UI de informacoes sobre um planeta
        __info_box = None                                #Caixa estatica para informacoes
        __info_name_text = None                   #Caixa do nome de um planeta nas informacoes
        __info_posx_text = None                   #Caixa da posicao X de um planeta nas informacoes
        __info_posz_text = None                   #Caixa da posicao Z de um planeta nas informacoes
        __info_mass_text = None                   #Caixa da massa de um planeta nas informacoes
        __info_trans_vx_text = None                #Caixa da velocidade de translacao em X de um planeta nas informacoes
        __info_trans_vz_text = None                #Caixa da velocidade de translacao em Z de um planeta nas informacoes
        __info_rot_text = None                   #Caixa da velocidade de rotacao de um planeta nas informacoes
        __info_radius_text = None                 #Caixa do raio de um planeta nas informacoes
        __info_color_combo = None
        
        __info_name_label = None                  #Label da caixa de nome nas informacoes
        __info_posx_label = None                  #Label da caixa de posicao X nas informacoes
        __info_posz_label = None                  #Label da caixa de posicao Z nas informacoes
        __info_mass_label = None                  #Label da caixa de massa nas informacoes
        __info_trans_vx_label = None               #Label da caixa de velocidade de translacao em X nas informacoes
        __info_trans_vz_label = None               #Label da caixa de velocidade de translacao em Z nas informacoes
        __info_rot_label = None                  #Label da caixa de velocidade de rotacao nas informacoes
        __info_radius_label = None                #Label da caixa de raio nas informacoes
        __info_color_label = None
        
        __save_body_btn = None                    #Botao para salvar edicoes feitas a um planeta
        __edit_body_btn = None                    #Botao para editar um planeta
        __remove_body_btn = None                  #Botao para remover um planeta
        __add_body_btn = None

        
        __BODY_UI_TITLE = 'Body Information'
        
        
        # Informacoes gerais da Interface
        __aRate = 0                                             #Velocidade de atualizacao da animacao
        __simulation = None                             #Um objeto Simulation, contendo a simulacao

        
        
        __COLORS_COMBO_CHOICES = [" ","Blue", "Cyan", "Gray", "Green", "Magenta", "Orange", "Red", "White", "Yellow"]
        __COLORS_MAP = [color.white, color.blue, color.cyan, color.gray, color.green, color.magenta, color.orange, color.red, color.white, color.yellow]

        # Basic size definition to create the GUI
        # Window width
        __W_WIDTH = 900
        # Window height
        __W_HEIGHT = 600
        # Simulation display width
        __S_WIDTH = 600
        # Simulation display height
        __S_HEIGHT = 400
        # Left margin space
        __L_MARGIN = 15
        # Top margin space
        __T_MARGIN = 15
        __W_MULT = 2
        d = 20
        __DISPLAY_TITLE = '3D Simulation'
        __CONTROL_TITLE = 'Simulation Controls'
        
        def __init__(self, title):
                # Create the main window which will hold all the GUI
                self.__window = window(width=self.__W_WIDTH, height=self.__W_HEIGHT, menus=True, title=title, style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.SIMPLE_BORDER)                
                
                #Chamada das funcoes de inicializacao
                self.__init_display__()
                self.__init_body_info_UI__()
##              self.initAddPlanetUI()
                self.__init_control_UI__()
##              self.__toggleAddPlanets(None)
##              
##              self.__window.panel.Bind(wx.EVT_KEY_DOWN, self.__onKeyPress)
##              self.__window.panel.Bind(wx.EVT_LEFT_DOWN, self.__onMouseClick)
        
        
        # Initializes the display to show the simulation
        def __init_display__(self):
                # Creates a static box to hold the simulation displayer
                self.__sim_box = wx.StaticBox(self.__window.win, 1, self.__DISPLAY_TITLE, pos=(self.__L_MARGIN, self.__T_MARGIN), size=(self.__S_WIDTH, self.__S_HEIGHT), style=wx.BORDER_DEFAULT)
                # Configures the basic settings of the simulation box
                window_color = wx.SystemSettings_GetColour(wx.SYS_COLOUR_MENUBAR)
                self.__sim_box.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)
                self.__sim_box.SetBackgroundColour(window_color)

                # Creates the displayer
                self.__disp = display(window=self.__window, x=self.__L_MARGIN+20, y=self.__T_MARGIN+20, width=self.__S_WIDTH-40, height=self.__S_HEIGHT-40, forward=-vector(0,1,2), style=wx.SIMPLE_BORDER)
                # Creates the simulation
                self.__simulation = OrbitSimulator(self.__disp)

                # Adjust scene settings   
                scene.autoscale = 0
                scene.range = 7
        
        
        # Initializes the basic GUI for the control of the simulation
        def __init_control_UI__(self):
              # The parent window for the objects
              parent = self.__window.panel
              # Static box to hold all the control objects
              self.__control_box = wx.StaticBox(self.__window.win, 1, self.__CONTROL_TITLE, pos = ((self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+self.__T_MARGIN)), size=(self.__S_WIDTH, self.__window.height-6*self.__T_MARGIN-self.__S_HEIGHT), style=wx.BORDER_DEFAULT)
              # Configure the static box settings
              window_color = wx.SystemSettings_GetColour(wx.SYS_COLOUR_MENUBAR)
              self.__control_box.SetBackgroundColour(window_color)
              # Create a pause button, rate slider, and orbits and labels radio buttons
              self.__pause_btn = wx.Button(parent, label=self.__PAUSE_TEXT, pos=((2*self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+3*self.__T_MARGIN)), size = (100, 30))
              self.__pause_btn.Bind(wx.EVT_BUTTON, self.__pause_simulation__)

              self.__rate_label = wx.StaticText(parent, pos=((2*self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+5.5*self.__T_MARGIN)), size=(100,20), label=self.__ARATE_SPEED_TEXT, style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
              self.__rate_slider = wx.Slider(parent, pos=((2*self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+6.7*self.__T_MARGIN)), size=(100,20), minValue=50, maxValue=1000)
              self.__rate_slider.SetValue(200)
              self.__rate_slider.Bind(wx.EVT_SLIDER, self.__set_animation_rate__)            
              
              self.__rb_orbits = wx.RadioBox(parent, label="Orbits", pos=((13*self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+2.5*self.__T_MARGIN)), size=(100,80), 
                 choices = ['Show', 'Hide'], style=wx.RA_SPECIFY_COLS)
              self.__rb_orbits.Bind(wx.EVT_RADIOBOX, self.__toggle_trail__)
            
              self.__rb_names = wx.RadioBox(parent, label="Labels", pos=((22*self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+2.5*self.__T_MARGIN)), size=(100,80),
                 choices = ['Show', 'Hide'], style=wx.RA_SPECIFY_ROWS)
              self.__rb_names.Bind(wx.EVT_RADIOBOX, self.__toggle_names__)
              
              # Set add body button
              self.__add_body_btn = wx.Button(parent, label='Add Body', pos=((31*self.__L_MARGIN), (self.__T_MARGIN+self.__S_HEIGHT+2.5*self.__T_MARGIN)), size=(90,25))
              self.__add_body_btn.Show()
              self.__add_body_btn.Bind(wx.EVT_BUTTON, self.__add_new_body__)
      

        #Funcao que inicializa a area de informacoes sobre um corpo celeste
        def __init_body_info_UI__(self):
              # The parent window for the objects
              parent = self.__window.panel
              # Static box to hold all the information about the body
              self.__info_box = wx.StaticBox(self.__window.win, 1, self.__BODY_UI_TITLE, pos=(self.__L_MARGIN+self.__S_WIDTH+self.__L_MARGIN, self.__T_MARGIN), size=(self.__window.width-self.__L_MARGIN-self.__S_WIDTH-2.5*self.__L_MARGIN, self.__W_HEIGHT-5*self.__T_MARGIN), style=wx.BORDER_DEFAULT)
              # Configure the static box settings
              window_color = wx.SystemSettings_GetColour(wx.SYS_COLOUR_MENUBAR)              
              self.__info_box.SetBackgroundColour(window_color)

              # Name information area       
              self.__info_name_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 3*self.__T_MARGIN), size=(100,20), style=wx.TE_READONLY)
              self.__info_name_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 3*self.__T_MARGIN), size=(60,20), label='Name: ', style= wx.ST_NO_AUTORESIZE)                                

              # Mass information area
              self.__info_mass_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 6*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 6*self.__T_MARGIN), size=(20,20), label='(kg)', style=wx.ST_NO_AUTORESIZE)
              self.__info_mass_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 6*self.__T_MARGIN), size=(50,20), label='Mass: ', style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)

              # Radius information area
              self.__info_radius_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 9*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 9*self.__T_MARGIN), size=(30,20), label='(Gm)', style=wx.ST_NO_AUTORESIZE)
              self.__info_radius_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 9*self.__T_MARGIN), size=(50,20), label='Radius: ', style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)

              # Position x area
              self.__info_posx_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 12*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 12*self.__T_MARGIN), size=(30,20), label='(Gm)', style=wx.ST_NO_AUTORESIZE)
              self.__info_posx_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 12*self.__T_MARGIN), size=(75,20), label='Position X: ', style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
              
              # Position z area
              self.__info_posz_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 15*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 15*self.__T_MARGIN), size=(30,20), label='(Gm)', style=wx.ST_NO_AUTORESIZE)
              self.__info_posz_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 15*self.__T_MARGIN), size=(75,20), label='Position Z: ', style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)

              # Translation velocity x area
              self.__info_trans_vx_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 18*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 18*self.__T_MARGIN), size=(40,20), label='(Gm/s)', style=wx.ST_NO_AUTORESIZE)              
              self.__info_trans_vx_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 18*self.__T_MARGIN), size=(80,20), label='Translation X: ', style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)

              # Translation velocity z area
              self.__info_trans_vz_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 21*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 21*self.__T_MARGIN), size=(40,20), label='(Gm/s)', style=wx.ST_NO_AUTORESIZE)              
              self.__info_trans_vz_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 21*self.__T_MARGIN), size=(80,20), label='Translation Z: ', style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
              
              # Rotation speed information area
              self.__info_rot_text = wx.TextCtrl(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 24*self.__T_MARGIN), size=(60,20), style=wx.TE_READONLY)       
              wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+13*self.__L_MARGIN, 24*self.__T_MARGIN), size=(40,20), label='(rad/s)', style=wx.ST_NO_AUTORESIZE)
              self.__info_rot_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 24*self.__T_MARGIN), size=(60,20), label='Rotation: ',  style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)

              # Color combo area   
              self.__info_color_combo = wx.ComboBox(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+8*self.__L_MARGIN, 27*self.__T_MARGIN), size=(100,20), choices=self.__COLORS_COMBO_CHOICES, style=wx.CB_READONLY | wx.CB_SORT)
              self.__info_color_combo.SetValue(self.__COLORS_COMBO_CHOICES[0])
              self.__info_color_label = wx.StaticText(parent, pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 27*self.__T_MARGIN), size=(60,20), label='Color: ',  style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
              self.__info_color_combo.Enable(False)
              
              # Set edit body button
              self.__edit_body_btn = wx.Button(parent, label='Edit Body', pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 33*self.__T_MARGIN), size=(90,25))
              self.__edit_body_btn.Bind(wx.EVT_BUTTON, self.__edit_selected_body__)

              # Set save edited body button
              self.__save_body_btn = wx.Button(parent, label='Save Edition', pos=(self.__L_MARGIN+self.__S_WIDTH+2*self.__L_MARGIN, 33*self.__T_MARGIN), size=(90,25))
              self.__save_body_btn.Bind(wx.EVT_BUTTON, self.__save_selected_body__)
              self.__save_body_btn.Hide()

              # Set remove body button
              self.__remove_body_btn = wx.Button(parent, label='Remove Body', pos=(self.__L_MARGIN+self.__S_WIDTH+9*self.__L_MARGIN, 33*self.__T_MARGIN), size=(90,25))
              self.__remove_body_btn.Hide()
              self.__remove_body_btn.Bind(wx.EVT_BUTTON, self.__remove_selected_body__)
      

        def __edit_selected_body__(self, evt):
              if self.__simulation.get_selected_body()[0] != None:
                      self.__edit_body_btn.Hide()
                      self.__save_body_btn.Show()
                      self.__remove_body_btn.Show()
                      self.__set_info_area_editable__(True)
                      self.__pause_simulation__(None)
                      self.__pause_btn.Enable(False)


        def __set_info_area_editable__(self, bool):
              self.__info_mass_text.SetEditable(bool)
              self.__info_radius_text.SetEditable(bool)
              self.__info_rot_text.SetEditable(bool)
              self.__info_trans_vx_text.SetEditable(bool)
              self.__info_trans_vz_text.SetEditable(bool)
              self.__info_posx_text.SetEditable(bool)
              self.__info_posz_text.SetEditable(bool)
              self.__info_name_text.SetEditable(bool)
              self.__info_color_combo.Enable(bool)
      
        def __clear_info_area_fields__(self):
              self.__info_mass_text.SetValue('')
              self.__info_radius_text.SetValue('')
              self.__info_rot_text.SetValue('')
              self.__info_trans_vx_text.SetValue('')
              self.__info_trans_vz_text.SetValue('')
              self.__info_posx_text.SetValue('')
              self.__info_posz_text.SetValue('')
              self.__info_name_text.SetValue('')
              self.__info_color_combo.SetValue(self.__COLORS_COMBO_CHOICES[0])

##      
##      def __onKeyPress(self, evt):
##              keycode = evt.GetKeyCode()
##              
##              if keycode == wx.WXK_SPACE:
##                      self.__pauseSimulation(None)
##              elif keycode == wx.WXK_PAGEUP:
##                      newRate = self.__rateSlider.GetValue()+50
##                      if newRate >= 2000:
##                              newRate = 2000
##                      self.__rateSlider.SetValue(newRate)
##                      self.__setAnimationRate(None)
##              elif keycode == wx.WXK_PAGEDOWN:
##                      newRate = self.__rateSlider.GetValue()-50
##                      if newRate <= 0:
##                              newRate = 0
##                      self.__rateSlider.SetValue(newRate)
##                      self.__setAnimationRate(None)
##              elif keycode == ord('O'):
##                      self.__rbOrbits.SetSelection(not(self.__rbOrbits.GetSelection()))
##                      self.__toggleTrail(None)
##              elif keycode == ord('N'):
##                      self.__rbNames.SetSelection(not(self.__rbNames.GetSelection()))
##                      self.__toggleNames(None)
##              elif keycode == ord('A'):
##                      self.__toggleAddPlanets(None)
##              
##              evt.Skip()
##      
        def __on_mouse_click__(self, evt):
              self.__w.panel.SetFocusIgnoringChildren()
              evt.Skip()
              return
              
      
        def __save_selected_body__(self, evt):
              base_message = "Incorrect field value at  "
              
              try:
                      error_at = "Position X."
                      posx = double(self.__info_posx_text.GetValue())
                      error_at = "Position Z."
                      posz = double(self.__info_posz_text.GetValue())
                      error_at = "Translation X."
                      velx = double(self.__info_trans_vx_text.GetValue())
                      error_at = "Translation Z."
                      velz = double(self.__info_trans_vz_text.GetValue())
                      error_at = "Mass."
                      mass = double(self.__info_mass_text.GetValue())
                      error_at = "Radius."
                      radius = double(self.__info_radius_text.GetValue())
                      error_at = "Nanme."
                      name = self.__info_name_text.GetValue()
                      error_at = "Rotation."
                      rots = double(self.__info_rot_text.GetValue())
                      error_at = "Color."
                      color = self.__get_color_choice__()
              except ValueError:
                      self.__error_message__(base_message+error_at)
                      return
              
              if type(self.__simulation.get_selected_body()[0] is Planet):
                      body = Planet(radius = 0, color = color, trans_velocity = vector(velx, 0, velz))
              elif type(self.__simulation.get_selected_body()[0] is Star):
                      body = Star(radius = 0, color = color)
                      
              body.set_position(vector(posx, 0, posz))
              body.set_mass(mass)
              body.set_radius(radius)
              body.set_name(name)
              body.set_rot_speed(rots)

              if(self.__adding_body == True):
                      self.simulation.add_planet(body)
                      self__adding_body = False
              else:
                      self.__simulation.edit_selected_body(body)
                            
              self.__save_body_btn.Hide()
              self.__edit_body_btn.Show()
              self.__remove_body_btn.Hide()
              self.__pause_simulation__(None)
              self.__pause_btn.Enable(True)
              self.__set_info_area_editable__(False)
      
        def __get_color_choice__(self):
              return self.__COLORS_MAP[self.__info_color_combo.GetCurrentSelection()]

              
        def __remove_selected_body__(self, evt):
              self.__simulation.remove_selected_body()
              self.__save_body_btn.Hide()
              self.__edit_body_btn.Show()
              self.__remove_body_btn.Enable()
              self.__set_info_area_editable__(False)
              self.__clear_info_area_fields__()

        def __add_new_body__(self, evt):
              self.__clear_info_area_fields__()
              self.__set_info_area_editable__(True)  
              self.__remove_body_btn.Hide()
              self.__edit_body_btn.Hide()
              self.__save_body_btn.Show()
              self.__adding_body = True
   
        def __set_animation_rate__(self, evt):
              self.__simulation.set_animation_rate(self.__rate_slider.GetValue())
              
        def __toggle_trail__(self, evt):
              choice = self.__rb_orbits.GetSelection()
              if choice == 0:
                      self.__simulation.show_trails(True)
              else:
                      self.__simulation.show_trails(False)
      
        def __toggle_names__(self, evt):
              choice = self.__rb_names.GetSelection()
              if choice == 0:
                      self.__simulation.show_names(True)
              else:
                      self.__simulation.show_names(False)
                      
        def __error_message__(self, message):
                wx.MessageBox(message, 'Data Error', wx.OK | wx.ICON_INFORMATION)

        def get_animation_rate(self):
              return self.__simulation.get_animation_rate()
      
      
##        def __get_color_combo_choice__(self):
##              return self.__info_color_combo.GetCurrentSelection()
      
      
        def __pause_simulation__(self, evt):
              paused = self.__simulation.is_paused()
              if paused:
                      self.__simulation.pause_simulation(False)
                      self.__pause_btn.SetLabel(self.__PAUSE_TEXT)
              else:
                      self.__simulation.pause_simulation(True)
                      self.__pause_btn.SetLabel(self.__CONTINUE_TEXT)

        # Set the body info of the selected body into the Information UI
        def __set_body_info__(self, body):
              self.__info_name_text.SetValue(str(body.get_name()))
              self.__info_mass_text.SetValue(str(body.get_mass()))
              self.__info_radius_text.SetValue(str(body.get_radius()))
              self.__info_posx_text.SetValue(str(body.get_position().x))
              self.__info_posz_text.SetValue(str(body.get_position().z))
              self.__info_rot_text.SetValue(str(body.get_rot_speed()))
              color_index = self.__COLORS_MAP.index(body.get_color())
              if color_index == 0:
                      color_index = self.__COLORS_COMBO_CHOICES.index("White")
              self.__info_color_combo.SetValue(self.__COLORS_COMBO_CHOICES[color_index])
              
              if type(body) is Planet:
                      self.__info_trans_vx_text.SetValue(str(body.get_trans_velocity().x))
                      self.__info_trans_vz_text.SetValue(str(body.get_trans_velocity().z))
              else:
                      self.__info_trans_vx_text.SetValue(str(0.0))
                      self.__info_trans_vz_text.SetValue(str(0.0))
              
        
        
        def runSimulation(self):
                self.__simulation.run()
                body, index = self.__simulation.get_selected_body()
              
                if self.__simulation.mouse_clicked():
                      self.__set_body_info__(body)
                      # Can't remove the Star
                      if not(type(body) is Star):
                              self.__remove_body_btn.Enable()
                      else:
                              self.__remove_body_btn.Disable()
              
                elif body != None and not (self.__simulation.is_paused()):
                      self.__set_body_info__(body)
