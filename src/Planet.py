from visual import *
from celestialBody import*
import numpy
import gc

class Planet(CelestialBody):
        # A planet body with all of its attributes and characteristics

        # Attribute holding its translation speed (around the star) in 3D-vector form
	__trans_velocity = vector(0,0,0)
	# Attribute holding its rotation speed in absolute form
	__rot_speed = 0
	# Attribute representing the aura of the planet (in order to be able to select it, due its small size)
	__aura = None
	# Attribute holding the trail of the orbit the planet makes when translading
	__orbit_trail = None

	
	def __init__(self, name='', pos=vector(0,0,0), radius=1.0, color = color.white, mass = 1, material = None, trans_velocity = vector(0,0,0), rot_speed=0):
		super(Planet, self).__init__(name=name, pos=pos, radius=radius, color=color, mass=mass, material=material)
		self.__trans_velocity = trans_velocity
		self.__rot_speed = rot_speed
		self.__aura = sphere(display=self.display, opacity=0.2)
		self.__orbit_trail = curve(display = self.display, radius = 0.1)
		self.__aura.pos = pos
		
		if radius*100 > 100:
			radius = 1
		self.__aura.radius = radius*100
		
		self.__aura.material = materials.emissive
		self.__orbit_trail.color = color
		self.__orbit_trail.visible = True
	
	def set_position(self, position):
		super(Planet, self).set_position(position)
		self.__aura.pos = position
		self.__orbit_trail.append(pos=position)
		
	def set_trans_velocity(self, trans_velocity):
		self.__trans_velocity = trans_velocity
	
	def set_rot_speed(self, rot_speed):
		self.__rot_speed = rot_speed
	
	def get_trans_velocity(self):
		return self.__trans_velocity
	
	def get_aura_radius(self):
		return self.__aura.radius
	
	def set_radius(self, radius):
		super(Planet, self).set_radius(radius)
		
		if (radius*100) > 100:
			radius = 1

		self.__aura.radius = radius*100
		
	def get_rot_speed(self):
		return self.__rot_speed
	
	def set_visible(self, bool):
		super(Planet, self).set_visible(bool)
		self.__aura.visible = bool
		self.__orbit_trail.visible = bool
		
	def leave_trail(self, bool):
		self.__orbit_trail.visible = bool
	
	def clear_planet(self):
		super(Planet, self).clean_body()
		self.__trans_velocity = vector(0,0,0)
		self.__rot_speed = 0
	
	def rotate(self, axis=(0,1,0), angle=0):
		super(Planet, self).rotate(axis=axis, angle=angle)
		self.__aura.rotate(aix=axis, angle=angle)
	
	def __del__(self):
		self.__aura.visible = False
		self.__orbit_trail.visible = False
		super(Planet, self).__del__()
		del self
		
