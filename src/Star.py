from celestialBody import *


class Star(CelestialBody):
        # A star, which is a celestial body with only rotation speed (by the definition of the program)


	# Attribute indicating the rotational speed of this star
	__rot_speed = 0
	
	def __init__(self, name='', pos=vector(0,0,0), radius=1.0, color = color.yellow, mass = 1, rot_speed=0):
		super(Star, self).__init__(name=name, pos=pos, radius=radius, color=color, mass=mass, material=materials.emissive)
		self.__rot_speed = rot_speed
	
	def set_rot_speed(self, rot_speed):
		self.__rot_speed = rot_speed
	
	def get_rot_speed(self):
		return self.__rot_speed
	
	def clear_star(self):
		super(Star, self).clear_body()
		self.__rot_speed = 0
	
