from visual import *

class CelestialBody(sphere):
        # A generic celestial body. It is inherited by classes like planet, star, comets, and others.

	
        # Attribute holding the name of the body
	__name = ''
        # Attribute containing the mass of the body      
	__mass = 0.01
	# Attribute with the label to be displayed over the body
	__label = None

	def __init__(self, name='', pos=vector(0,0,0), radius=1.0, color = color.white, mass = 0.01, material = None):
		super(CelestialBody, self).__init__(pos = pos, radius = radius, color=color, material=material)
		self.__name = name
		self.__mass = mass
		
		if name != '': # if the name is not empty, create the label
			self.__label = label(display = self.display, pos=pos, text=name, txoffset=2+radius, yoffset=12, space=0.6, height=10, border=6, font='sans')
	
	def set_name(self, name):
		self.__name = name

		if name == '': #the name is empty
                        self.show_name(false)
                        self.__label = None
                else: #the name is not empty
        		if self.__label != None: #if the label is not empty, change its text with the new name
                		self.__label.text = name
                        else: #otherwise, create a new label with the new name
                                self.__label = label(display = self.display, pos=self.pos, text=name, txoffset=2+self.radius, yoffset=12, space=0.6, height=10, border=6, font='sans')
	
	def set_visible(self, bool):
		self.visible = bool
		if self.__label != None:
        		self.__label.visible = bool
	
	def set_position(self, position):
		self.pos = position
		
		if self.__label != None:
			self.__label.pos = position
	
	def set_radius(self, radius):
		self.radius = radius
	
	def set_color(self, color):
		self.color = color
	
	def set_mass(self, mass):
		self.__mass = mass
	
	def set_material(self, material):
		self.material = material
	
	def get_name(self):
		return self.__name
	
	def get_position(self):
		return self.pos
	
	def get_radius(self):
		return self.radius
	
	def get_color(self):
		return self.color
	
	def get_mass(self):
		return self.__mass
	
	def get_material(self):
		return self.material
	
	def show_name(self, bool):
                if self.__label != None:
        		self.__label.visible = bool
		
	def clear_body(self):
		self.__nanme = ""
		self.__mass = 0
		self.__label = None
		self.radius = 0
		self.pos = vector(0,0,0)
		self.color = None
	
	def __del__(self):
		self.setVisible(False)
		del self
		
