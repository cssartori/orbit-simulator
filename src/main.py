#from planetSimulatorGui import *
from visual import *
from visual.graph import *
import wx
from Planet import *
from OrbitSimulator import *
from OrbitSimulatorGui import *

__BASE_SIZE = 500
__HGRAPH = 100
__WIDTH = __BASE_SIZE+__HGRAPH
__HEIGHT = __BASE_SIZE-__HGRAPH
__W_MULT = 2
d = 20
title = 'simm'
'''
Por se tratar de uma linguagem compilada,  e de necessitar o pacote
visual python instalado (funcional apenas no Windows, ou com o Wine no Linux),
nao temos um makefile. 
Havendo um interpretador python compativel e o pacote instalado o programa
rodara corretamente.
'''

if __name__ == '__main__':
        #define um objetivo do tipo interface grafica
        win=OrbitSimulatorGui(title="Orbit Simulator")
        while 1:
                rate(win.get_animation_rate()) # stops the computation for 1/x seconds
                win.runSimulation()
        #win = window(width=1000, height=600, menus=True, title=title, style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.SIMPLE_BORDER)             
        #disp = display(window=win, x=d+(d/4), y=d+(d/4), width=__WIDTH-d, height=__HEIGHT-(2)*d, forward=-vector(0,1,2), style=wx.SIMPLE_BORDER)
        #simulation=OrbitSimulator(disp)
        #scene.autoscale = 0
        #scene.range = 7
        #loop infinito para manter a simulacao em execucao
        
