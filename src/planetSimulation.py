from Planet import *
from Star import *
from functools import partial


class planetSimulation():

        # Gravitational constant G = 6.67384x10^-11 m^3/kgs^2
	__G = 6.67384e-11
	# The number of meter that each unit of the VPython scene corresponds to - 10^9 meters in this case
	__METERS_PER_UNIT = 1e9
	# Number of seconds that each steps corresponds to - in this case 2000 sec.
	__SEC_PER_STEP = 2000
	# Number of steps per frame of the VPython scene - in this case 1000
	__STEPS_PER_FRAME = 1000
	# A list of all planets currently in the scene
	__planets = []
        # An attribute containing the system's star information
        __star = None
        
	# A variable containing the information of a selected body in the simulation (by the mouse)
	__selected_body = (None, 0)
	# A flag variable indicating if the mouse was clicked and selected anything from the scene
	__mouse_clicked = False
        # A variable indicating if the simulation is paused or not
        __paused = False
        
	# The simulation scene itself
	__scene = None
	
	
	def __init__(self, scene):
		self.__scene = scene
		
		self.__star = Star(name="Sun", radius=6.955, mass=1.988435e30, rot_speed = 2.904e-6)
		self.__planets.append(Planet(name="Mercury", pos=vector(69.81,0,0), radius = 0.024, mass=3.30e23, trans_velocity = vector(0,0,4.74e-5), rot_speed=1.24e-6))
		self.__planets.append(Planet(name="Venus", pos=vector(108.8,0,0), radius = 0.060, mass=4.86e24, trans_velocity = vector(0,0,3.5e-5), rot_speed=2.99246e-7))
		self.__planets.append(Planet(name="Earth", pos=vector(152.097,0,0), radius = 0.063, mass=5.97e24, material=materials.earth, trans_velocity = vector(0,0,2.98e-5), rot_speed=7.292115e-5))
		self.__planets.append(Planet(name="Mars", pos=vector(249.228,0,0), radius = 0.033, mass=6.41e23, trans_velocity = vector(0,0,2.41e-5), rot_speed=7.08821804e-5))
		self.__planets.append(Planet(name="Jupiter", pos=vector(816.083,0,0), radius = 0.6197, mass=1.89e27, trans_velocity = vector(0,0,1.3e-5), rot_speed=1.7585e-4))
		self.__planets.append(Planet(name="Saturn", pos=vector(1503.98,0,0), radius = 0.57316, mass=5.68e26, trans_velocity = vector(0,0,0.964e-5), rot_speed=1.6378e-4))
		self.__planets.append(Planet(name="Uranus", pos=vector(3006.38,0,0), radius = 0.57316, mass=8.68e25, trans_velocity = vector(0,0,0.68e-5), rot_speed=1.0124e-4))
		self.__planets.append(Planet(name="Neptune", pos=vector(4536.87,0,0), radius = 0.57316, mass=1.02e26, trans_velocity = vector(0,0,0.543e-5), rot_speed=1.0834e-4))
	
		
	def add_planet(self, new_planet):
		self.__planets.append(new_planet)
	
	def edit_selected_body(self, body):
		if type(self.__selected_body[0]) is Star:
                        # first delete completely the old star
			del_body = self.__star
			del del_body

			# set the star to be another one
			self.__star = body
			self.__selectedBody = (self.__star, -1)
		else: # if the body is not a star, it is a planet
                        # first delete the old planet
                        del_body = self.__planets[self.__selected_body[1]]
        		del del_body

                	# set the old planet position as the new edited planet
                        self.__planets[self.__selected_body[1]] = body
        		self.__selected_body = (self.__planets[self.__selected_body[1]], self.__selected_body[1])
		
		return
		
	def remove_selected_body(self):
		body = self.__planets.pop(self.__selected_body[1])
		self.__selected_body = (None, -1)
		del body
		
	# Map function applies a function to all elements in the given list on a recursive manner
	def __map__(self, function, list):
		if not list: #when the list is empty
			return []
                # take the first item in the list and create a new empty list
		item = list.pop(0)
		new_list = []
                # applies the function to the item
		function(item)
		# insert the modified item in the new list
		new_list.append(item)
		
		#recursively call the map function
		return new_list+self.__map__(function, list)

        # Function that calculates the acceleration of a given planet given all the other planets, as well as the system's star, based in the basic gravitational force equation
	def __acceleration__(self, planet, p_pos):
                # planet: the planet to calculate the accelaration
                # p_pos: the current position of this planet (specially used because of the RK method)
                
                # calculate the acceleration from scratch
		acc=vector(0,0,0)
		for p in self.__planets:
			if p==planet:	# The planet does not has a force on itself
				continue
			# calculate the distance and the force of the current planet on it
			dist = (p.get_position()-p_pos)*self.__METERS_PER_UNIT
			acc+=(self.__G*p.get_mass()*dist)/mag(dist)**3

		# lastly, calculate the force the star has over this planet
		dist = 	(self.__star.get_position()-p_pos)*self.__METERS_PER_UNIT
		acc += self.__G*self.__star.get_mass()*dist/mag(dist)**3
		
		return acc
	
	# Translation step of a planet. It uses the 4th order Runge-Kutta method for solving ODEs in order to minimize the total error
	def __translation_step__(self, planet):
		
		dt=self.__SEC_PER_STEP
		dt_2=dt/2
		
		# ------------K1------------------
                # RK 1st term
		dist = (self.__star.get_position()-planet.get_position())*self.__METERS_PER_UNIT
		
		k1a = self.__acceleration__(planet, planet.get_position())/self.__METERS_PER_UNIT
		k1v = planet.get_trans_velocity()
		
		pos1 = planet.get_position() + dt_2*k1v
		vel1 = planet.get_trans_velocity() + dt_2*k1a
		
		#------------K2------------------
		# RK 2nd term
		dist = (self.__star.get_position()-pos1)*self.__METERS_PER_UNIT
		
		k2a = self.__acceleration__(planet, pos1)/self.__METERS_PER_UNIT
		k2v = vel1
		
		pos2 = planet.get_position() + dt_2*k2v
		vel2 = planet.get_trans_velocity() + dt_2*k2a
		
		#------------K3------------------
		# RK 3rd term
		dist = (self.__star.get_position()-pos2)*self.__METERS_PER_UNIT
		
		k3a = self.__acceleration__(planet, pos2)/self.__METERS_PER_UNIT
		k3v = vel2
		
		pos3 = planet.get_position() + dt*k3v
		vel3 = planet.get_trans_velocity() + dt*k3a
		
		#------------K4------------------
		# RK 4th term
		dist = (self.__star.get_position()-pos3)*self.__METERS_PER_UNIT
		
		k4a = self.__acceleration__(planet, pos3)/self.__METERS_PER_UNIT
		k4v = vel3
		
                #--------------------------------

		# Final calculations considering all the terms (according to the RK-4 method)
		pos = planet.get_position()+dt/6*(k1v+2*k2v+2*k3v+k4v)
		vel = planet.get_trans_velocity()+dt/6*(k1a+2*k2a+2*k3a+k4a)

		# Set the new values of position and velocity of the planet
		planet.set_position(pos)
		planet.set_trans_velocity(vel)
		
	# Rotation step of a body. It rotates a body according to its rotation speed.		
	def __rotation_step__(self, body):
		body.rotate(axis=(0,1,0), angle=body.get_rot_speed()*self.__SEC_PER_STEP)

	# A step a planet: translation and rotation.
	def __planet_step__(self, planet):
		self.__translation_step__(planet)
		self.__rotation_step__(planet)

	# Turn on or off the trails of the planets
	def show_trails(self, bool):
		f = lambda b: lambda p: p.leave_trail(b)
		g = f(bool)
		self.__planets = self.__map__(g, self.__planets)

	# Turn on or off the labels of the bodies	
	def show_names(self, bool):
		f = lambda b: lambda p: p.show_name(b)
		g = f(bool)
		self.__planets = self.__map__(g, self.__planets)
		self.__sun.show_name(bool)

	# Pause/continue the simulation
	def pause_simulation(self, bool):
		self.__paused = bool

	# Checks wheter the simulation is paused or not
	def is_paused(self):
		return self.__paused

	# Verifies if the location clicked by the mouse is a planet or the star. If it is one of them, returns the corresponding body, as well as its index. Particularly for the star, its index is -1.
	def __verify_clicked_location__(self, location):
		sphere_eq = 0
		index=0

		# check if the star has been selected
		pos=self.__star.get_position()
		radius_x_10 = lambda r: r*10
		r=radius_x_10(self.__star.get_radius())
		sphere_eq = ((location.x - pos.x)**2)+(0)+((location.z - pos.z)**2)
			
		if(sphere_eq <= r**2): # the star has been selected
			return (self.__star, -1)

		# check if one of the planets has been selected
		for planet in self.__planets:
			pos=planet.get_position()
			r=radius_x_10(planet.get_aura_radius())
			sphere_eq = ((location.x - pos.x)**2)+(0)+((location.z - pos.z)**2)
			
			if(sphere_eq <= r**2): # planet has been selected
				return (planet, index)
				
			index+=1
			
                # if none body has been selected
		return (None, -1)
	
	def get_selected_body(self):
		return self.__selected_body
		
	def mouse_clicked(self):
		return self.__mouseClicked
	
	#Funcao que de fato executa um "passo" na simulacao, chamando todas as funcoes necessarias
	#para tal acao
	def run(self):
		if not self.is_paused():
			self.__planets = self.__map__(self.__planet_step__, self.__planets)
			self.__rotation_step__(self.__star)
			
		self.__mouse_clicked = False

		# if mouse has been clicked
		if self.__scene.mouse.events:
			m = self.__scene.mouse.getclick()
			if(m.click=='left'): # if the left button has been clicked
                                # get click location
        			location = m.project(normal=(0,1,0))
				self.__selected_body = self.__verify_clicked_location__(location)
				if(self.__selected_body[0] != None): # if something has indeed been clicked set the flag
					self.__mouse_clicked = True
					
		return
		
