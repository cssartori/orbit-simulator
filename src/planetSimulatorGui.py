from visual import *
from visual.graph import *
import wx
from Planet import *
from planetSimulation import *
from GenList import *


# Basic size definition to create the GUI
__BASE_SIZE = 500
__HGRAPH = 100
__WIDTH = BASE_SIZE+HGRAPH
__HEIGHT = BASE_SIZE-HGRAPH
__W_MULT = 2
d = 20


class planetSimulatorGui():
	# An orbit simulator GUI with all its components and interactions

	# A simple window to contain all the GUI
	__window = None
	# An static box to hold the simulation displayer
	__sim_box = None
	# A 3D display of the simulation
	__disp = None
	
	# 
	__addDownPlanetBtn = None
	__posXaddArea = None			#Caixa de entrada da posicao X de um planeta
	__posZaddArea = None			#Caixa de entrada da posicao Z de um planeta
	__massAddArea = None			#Caixa de entrada da massa de um planeta
	__radiusAddArea = None			#Caixa de entrada do raio de um planeta
	__planetColorCombo = None		#Combo com lista de cores para os planetas
	__transVXaddArea = None			#Caixa de entrada da velocidade de translacao em X de um planeta
	__transVZaddArea = None			#Caixa de entrada da velocidade de translacao em Z de um planeta
	__rotSaddArea = None			#Caixa de entrada da velocidade de rotacao de um planeta
	__nameAddArea = None			#Caixa de entrada do nome de um planeta
	
	__nameAddLabel = None			#Label da caixa de nome
	__posXaddLabel = None			#Label da caixa de entrada X
	__posZaddLabel = None			#Label da caixa de entrada Z
	__massAddLabel = None			#Label da caixa de entrada da Massa
	__radiusAddLabel = None			#Label da caixa de entrada do Raio
	__transVXaddLabel = None		#Label da caixa de entrada da velocidade X
	__transVZaddLabel = None		#Label da caixa de entrada da velocidade Z
	__rotSaddLabel = None			#Label da caixa de entrada da velocidade de rotacao
	__colorComboLabel = None		#Label do combo de cores
	
	__planetAddBtn = None			#Botao de adicao de planetas
	__addDownPlanetBtn = None		#Botao que mostra as opcoes para adicao de planetas (inicialmente escondidas)
	
	__addingPlanets = True			#Flag indicando se planetas estao sendo adicionados
	
	
	# UI de controles da simulacao
	__pauseBtn = None				#Botao de pausa
	__rbNames = None				#Botoes de radio para mostrar/esconder nomes de planetas
	__rbOrbits = None				#Botoes de radio para mostrar/esconder orbitas de planetas
	__rateSlider = None				#slider para escolher velocidade da animacao
	__controlBox = None				#Caixa estatica para os controles
	
	
	# UI de informacoes sobre um planeta
	__infoBox = None				#Caixa estatica para informacoes
	__infoNameArea = None			#Caixa do nome de um planeta nas informacoes
	__infoPosXarea = None			#Caixa da posicao X de um planeta nas informacoes
	__infoPosZarea = None			#Caixa da posicao Z de um planeta nas informacoes
	__infoMassArea = None			#Caixa da massa de um planeta nas informacoes
	__infoTransVXarea = None		#Caixa da velocidade de translacao em X de um planeta nas informacoes
	__infoTransVZarea = None		#Caixa da velocidade de translacao em Z de um planeta nas informacoes
	__infoRotSarea = None			#Caixa da velocidade de rotacao de um planeta nas informacoes
	__infoRadiusArea = None			#Caixa do raio de um planeta nas informacoes

	__infoNameLabel = None			#Label da caixa de nome nas informacoes
	__infoPosXlabel = None			#Label da caixa de posicao X nas informacoes
	__infoPosZlabel = None			#Label da caixa de posicao Z nas informacoes
	__infoMassLabel = None			#Label da caixa de massa nas informacoes
	__infoTransVXlabel = None		#Label da caixa de velocidade de translacao em X nas informacoes
	__infoTransVZlabel = None		#Label da caixa de velocidade de translacao em Z nas informacoes
	__infoRotSlabel = None			#Label da caixa de velocidade de rotacao nas informacoes
	__infoRadiusLabel = None		#Label da caixa de raio nas informacoes
	
	__saveBodyBtn = None			#Botao para salvar edicoes feitas a um planeta
	__editBodyBtn = None			#Botao para editar um planeta
	__removeBodyBtn = None			#Botao para remover um planeta
	
	
	
	# Informacoes gerais da Interface
	__aRate = 0						#Velocidade de atualizacao da animacao
	__simulation = None				#Um objeto Simulation, contendo a simulacao

	
	
	__COLOR_COMBO_CHOICES = [" ","Amarelo", "Azul", "Branco", "Ciano", "Cinza", "Laranja", "Magenta", "Verde", "Vermelho"]

	__COLORS_MAP = GenList()
	__COLORS_MAP.setItem(color.white)
	__COLORS_MAP.append(color.yellow)
	__COLORS_MAP.append(color.blue)
	__COLORS_MAP.append(color.white)
	__COLORS_MAP.append(color.cyan)
	__COLORS_MAP.append(color.gray)
	__COLORS_MAP.append(color.orange)
	__COLORS_MAP.append(color.magenta)
	__COLORS_MAP.append(color.green)
	__COLORS_MAP.append(color.red)
	
	
	
	
	def __init__(self, title):
		
		#Criacao de uma instancia Janela, a qual contera todos objetos da UI
		self.__window = window(width=__W_MULT*(__BASE_SIZE+window.dwidth), height=(__BASE_SIZE+window.dheight+window.menuheight+__HGRAPH), menus=True, title=title, style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.SIMPLE_BORDER)		
		self.__aRate=200
		
		#Chamada das funcoes de inicializacao
		self.initDisplay()
		self.initBodyInfoUI()
		self.initAddPlanetUI()
		self.initControlUI()
		self.__toggleAddPlanets(None)
		
		self.__window.panel.Bind(wx.EVT_KEY_DOWN, self.__onKeyPress)
		self.__window.panel.Bind(wx.EVT_LEFT_DOWN, self.__onMouseClick)
	
	
	#Funcao que inicializa o display para mostrar a simulacao
	def initDisplay(self):
		
		self.__sim_box = wx.StaticBox(self.__window.win, 1, 'Simulacao 3D', (15, 5), size=(__WIDTH, __HEIGHT), style=wx.BORDER_DEFAULT)
		
		windowColor = wx.SystemSettings_GetColour(wx.SYS_COLOUR_MENUBAR)
		
		self.__sim_box.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)

		self.__sim_box.SetBackgroundColour(windowColor)
		
		self.__disp = display(window=self.__window, x=d+(d/4), y=d+(d/4), width=__WIDTH-d, height=__HEIGHT-(2)*d, forward=-vector(0,1,2), style=wx.SIMPLE_BORDER)

		self.__simulation=planetSimulation(self.__disp)
		   
		scene.autoscale = 0
		scene.range = 7
	
	
	#Funcao que inicializa a interface para adicao de planetas
	def initAddPlanetUI(self):
                pass
##		p = self.__window.panel
##		
##		self.__addDownPlanetBtn = wx.Button(p, label='Adicionar Planeta ...', pos=(__WIDTH+d*2,d), size=(3*__HGRAPH+3*d, 20))
##		self.__addDownPlanetBtn.Bind(wx.EVT_BUTTON, self.__toggleAddPlanets)
##		
##		self.__posXaddArea = wx.TextCtrl(p, pos=(__WIDTH+1*__HGRAPH+d,__HGRAPH),
##								size=(50,20))						
##												
##		self.__posZaddArea = wx.TextCtrl(p, pos=(__WIDTH+3*__HGRAPH,__HGRAPH),
##								size=(50,20))
##		
##		self.__massAddArea = wx.TextCtrl(p, pos=(__WIDTH+1*__HGRAPH+d,__HGRAPH/2),
##								size=(50,20))
##		
##		self.__radiusAddArea = wx.TextCtrl(p, pos=(__WIDTH+3*__HGRAPH,__HGRAPH/2),
##								size=(50,20))
##		
##		self.__posXaddLabel = wx.StaticText(p, pos=(__WIDTH+1*__HGRAPH/2-d/2,__HGRAPH+2), size=(70,20), label='Posicao X: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##							
##		self.__posZaddLabel = wx.StaticText(p, pos=(__WIDTH+2*__HGRAPH+d,__HGRAPH+2), size=(70,20), label='Posicao Z: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__massAddLabel = wx.StaticText(p, pos=(__WIDTH+1*__HGRAPH/2-d/2,__HGRAPH/2+2), size=(50,20), label='Massa: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__radiusAddLabel = wx.StaticText(p, pos=(__WIDTH+2*__HGRAPH+d,__HGRAPH/2+2), size=(50,20), label='Raio: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__planetAddBtn = wx.Button(p, label='Adicionar Planeta', pos=(__WIDTH+2*__HGRAPH+d,__HGRAPH*5/2+2), size=(135,20))
##		self.__planetAddBtn.Bind(wx.EVT_BUTTON, self.__addPlanet)
##		
##		self.__planetColorCombo = wx.ComboBox(p, pos=(__WIDTH+3*__HGRAPH-2*d,__HGRAPH*2+2), size=(90,20), choices=self.__COLOR_COMBO_CHOICES, style=wx.CB_READONLY | wx.CB_SORT)
##		self.__planetColorCombo.SetValue(self.__COLOR_COMBO_CHOICES[0])
##		
##		self.__colorComboLabel = wx.StaticText(p, pos=(__WIDTH+2*__HGRAPH+d,__HGRAPH*2+2), size=(30,20), label='Cor: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__transVXaddArea = wx.TextCtrl(p, pos=(__WIDTH+1*__HGRAPH+d,__HGRAPH*3/2),
##														size=(50,20))
##		
##		self.__transVXaddLabel = wx.StaticText(p, pos=(__WIDTH+1*__HGRAPH/2-d/2,__HGRAPH*3/2+2), size=(80,20), label='Translacao X: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__transVZaddLabel = wx.StaticText(p, pos=(__WIDTH+2*__HGRAPH+d,__HGRAPH*3/2+2), size=(80,20), label='Translacao Z: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##							
##		self.__transVZaddArea = wx.TextCtrl(p, pos=(__WIDTH+3*__HGRAPH,__HGRAPH*3/2+2),
##														size=(50,20))
##		
##		self.__rotSaddArea = wx.TextCtrl(p, pos=(__WIDTH+1*__HGRAPH+d,__HGRAPH*2+2),
##														size=(50,20))
##	
##		self.__rotSaddLabel = wx.StaticText(p, pos=(__WIDTH+1*__HGRAPH/2-d/2,__HGRAPH*2+2), size=(60,20), label='Rotacao: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__nameAddArea = wx.TextCtrl(p, pos=(__WIDTH+1*__HGRAPH+d,__HGRAPH*5/2+2),
##														size=(50,20))
##		
##		self.__nameAddLabel = wx.StaticText(p, pos=(__WIDTH+1*__HGRAPH/2-d/2,__HGRAPH*5/2+2), size=(60,20), label='Nome: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
	
	#Funcao que inicializa a area de controle da animacao
	def initControlUI(self):
                pass
##		p=self.__window.panel
##		
##		self.__controlBox = wx.StaticBox(self.__window.win, 1, 'Controles da Simulacao', pos = (__WIDTH+d*2, (__HGRAPH*3+2*d)), size=(__WIDTH/2 + __HGRAPH/2, self.__window.height-__HEIGHT-window.dheight+window.menuheight+d/2), style=wx.BORDER_DEFAULT)
##		
##		windowColor = wx.SystemSettings_GetColour(wx.SYS_COLOUR_MENUBAR)
##		
##		self.__controlBox.SetBackgroundColour(windowColor)
##		
##		self.__rbOrbits = wx.RadioBox(p, label="Orbitas", pos=(__WIDTH+1*__HGRAPH,5*__HGRAPH), size=(100,80),
##                 choices = ['Mostrar', 'Esconder'], style=wx.RA_SPECIFY_ROWS)
##		self.__rbOrbits.Bind(wx.EVT_RADIOBOX, self.__toggleTrail)
##		
##		self.__rbNames = wx.RadioBox(p, label="Identificadores", pos=(__WIDTH+2.5*__HGRAPH,5*__HGRAPH), size=(100,80),
##                 choices = ['Mostrar', 'Esconder'], style=wx.RA_SPECIFY_ROWS)
##		self.__rbNames.Bind(wx.EVT_RADIOBOX, self.__toggleNames)
##		
##		self.__rateLabel = wx.StaticText(p, pos=(WIDTH+1*HGRAPH,4.5*HGRAPH-d), size=(60,20), label='Velocidade: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__rateSlider = wx.Slider(p, pos=(WIDTH+1*HGRAPH,4.5*HGRAPH), size=(WIDTH/3+d*2.5,20), minValue=50, maxValue=1000)
##		self.__rateSlider.SetValue(200)
##		self.__rateSlider.Bind(wx.EVT_SLIDER, self.__setAnimationRate)
##							
##		self.__pauseBtn = wx.Button(p, label='Pausar', pos=(WIDTH+1.75*HGRAPH,3.5*HGRAPH+d))
##		self.__pauseBtn.Bind(wx.EVT_BUTTON, self.__pauseSimulation)
		
		
	
	#Funcao que inicializa a area de informacoes sobre um corpo celeste
	def initBodyInfoUI(self):
                pass
##		p = self.__w.panel
##		
##		self.__infoBox = wx.StaticBox(self.__w.win, 1, 'Informacoes de Corpo Celeste', (15, (HGRAPH*4)+d*1.5), size=(WIDTH, self.__w.height-HEIGHT-window.dheight+window.menuheight-HGRAPH+d), style=wx.BORDER_DEFAULT)
##		
##		windowColor = wx.SystemSettings_GetColour(wx.SYS_COLOUR_MENUBAR)
##		
##		self.__infoBox.SetBackgroundColour(windowColor)
##		
##		self.__infoMassArea = wx.TextCtrl(p, pos=(d*5.5,(HGRAPH*4)+d*3),
##								size=(60,20), style=wx.TE_READONLY)
##		
##		wx.StaticText(p, pos=(d*9,(HGRAPH*4)+d*3), size=(20,20), label='(kg)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoMassLabel = wx.StaticText(p, pos=(d+d/2,(HGRAPH*4)+d*3), size=(50,20), label='Massa: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__infoRadiusArea = wx.TextCtrl(p, pos=((d*10+110),(HGRAPH*4)+d*3),
##								size=(60,20), style=wx.TE_READONLY)
##								
##		wx.StaticText(p, pos=(d*19,(HGRAPH*4)+d*3), size=(30,20), label='(Gm)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoRadiusLabel = wx.StaticText(p, pos=((d*10+80)-50,(HGRAPH*4)+d*3), size=(50,20), label='Raio: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__infoPosXarea = wx.TextCtrl(p, pos=(d*5.5,(HGRAPH*4)+d*5),
##								size=(60,20), style=wx.TE_READONLY)
##		
##		wx.StaticText(p, pos=(d*9,(HGRAPH*4)+d*5), size=(30,20), label='(Gm)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoPosXlabel = wx.StaticText(p, pos=(d+d/2,(HGRAPH*4)+d*5), size=(70,20), label='Posicao X: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__infoPosZarea = wx.TextCtrl(p, pos=((d*10+110),(HGRAPH*4)+d*5),
##								size=(60,20), style=wx.TE_READONLY)
##		
##		wx.StaticText(p, pos=(d*19,(HGRAPH*4)+d*5), size=(30,20), label='(Gm)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoPosZlabel = wx.StaticText(p, pos=((d*10+80)-50,(HGRAPH*4)+d*5), size=(70,20), label='Posicao Z: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__infoTransVXarea = wx.TextCtrl(p, pos=(d*5.5,(HGRAPH*4)+d*7),
##								size=(60,20), style=wx.TE_READONLY)
##		
##		wx.StaticText(p, pos=(d*9,(HGRAPH*4)+d*7), size=(40,20), label='(Gm/s)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoTransVXlabel = wx.StaticText(p, pos=(d+d/2,(HGRAPH*4)+d*7), size=(70,20), label='Translacao X: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__infoTransVZarea = wx.TextCtrl(p, pos=((d*10+110),(HGRAPH*4)+d*7),
##								size=(60,20), style=wx.TE_READONLY)
##		
##		wx.StaticText(p, pos=(d*19,(HGRAPH*4)+d*7), size=(40,20), label='(Gm/s)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoTransVZlabel = wx.StaticText(p, pos=((d*10+85)-50,(HGRAPH*4)+d*7), size=(70,20), label='Translacao Z: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		self.__infoRotSarea = wx.TextCtrl(p, pos=((d*20+100),(HGRAPH*4)+d*7),
##								size=(60,20), style=wx.TE_READONLY)
##		
##		self.__infoRotSlabel = wx.StaticText(p, pos=((d*20+85)-50,(HGRAPH*4)+d*7), size=(60,20), label='Rotacao: ',
##							style=wx.ALIGN_CENTRE | wx.ST_NO_AUTORESIZE)
##		
##		wx.StaticText(p, pos=(d*28.5,(HGRAPH*4)+d*7), size=(40,20), label='(rad/s)',
##							style=wx.ST_NO_AUTORESIZE)
##		
##		self.__infoNameLabel = wx.StaticText(p, pos=(d+d/2,(HGRAPH*4.5)+d*6), size=(60,20), label='Nome: ',
##							style= wx.ST_NO_AUTORESIZE)
##		self.__infoNameLabel.Hide()
##							
##		self.__infoNameArea = wx.TextCtrl(p, pos=(d*5.5,(HGRAPH*4.5)+d*6),
##								size=(60,20), style=wx.TE_READONLY)
##		self.__infoNameArea.Hide()
##		
##		self.__editBodyBtn = wx.Button(p, label='Editar Corpo', pos=(d*20,(HGRAPH*4.5)+d*6), size=(90,25))
##		self.__editBodyBtn.Bind(wx.EVT_BUTTON, self.__editSelectedBody)
##		
##		self.__saveBodyBtn = wx.Button(p, label='Salvar Edicao', pos=(d*20,(HGRAPH*4.5)+d*6), size=(90,25))
##		self.__saveBodyBtn.Bind(wx.EVT_BUTTON, self.__saveSelectedBody)
##		
##		self.__removeBodyBtn = wx.Button(p, label='Remover Corpo', pos=(d*25,(HGRAPH*4.5)+d*6), size=(90,25))
##		self.__removeBodyBtn.Bind(wx.EVT_BUTTON, self.__removeSelectedBody)
	
	
	def __editSelectedBody(self, evt):
		if self.__simulation.getSelectedBody()[0] != None:
			self.__editBodyBtn.Hide()
			self.__saveBodyBtn.Show()
			
			self.__infoNameArea.Show()
			self.__infoNameLabel.Show()
			
			self.__setInfoAreaEditable(True)

	def __setInfoAreaEditable(self, bool):
		self.__infoMassArea.SetEditable(bool)
		self.__infoRadiusArea.SetEditable(bool)
		self.__infoRotSarea.SetEditable(bool)
		self.__infoTransVXarea.SetEditable(bool)
		self.__infoTransVZarea.SetEditable(bool)
		self.__infoPosXarea.SetEditable(bool)
		self.__infoPosZarea.SetEditable(bool)
		self.__infoNameArea.SetEditable(bool)
	
	def __clearInfoAreaFields(self):
		self.__infoMassArea.SetValue('')
		self.__infoRadiusArea.SetValue('')
		self.__infoRotSarea.SetValue('')
		self.__infoTransVXarea.SetValue('')
		self.__infoTransVZarea.SetValue('')
		self.__infoPosXarea.SetValue('')
		self.__infoPosZarea.SetValue('')
		self.__infoNameArea.SetValue('')
		
		self.__infoBox.SetLabel("Informacoes de Corpo Celeste")
	
	def __onKeyPress(self, evt):
		keycode = evt.GetKeyCode()
		
		if keycode == wx.WXK_SPACE:
			self.__pauseSimulation(None)
		elif keycode == wx.WXK_PAGEUP:
			newRate = self.__rateSlider.GetValue()+50
			if newRate >= 2000:
				newRate = 2000
			self.__rateSlider.SetValue(newRate)
			self.__setAnimationRate(None)
		elif keycode == wx.WXK_PAGEDOWN:
			newRate = self.__rateSlider.GetValue()-50
			if newRate <= 0:
				newRate = 0
			self.__rateSlider.SetValue(newRate)
			self.__setAnimationRate(None)
		elif keycode == ord('O'):
			self.__rbOrbits.SetSelection(not(self.__rbOrbits.GetSelection()))
			self.__toggleTrail(None)
		elif keycode == ord('N'):
			self.__rbNames.SetSelection(not(self.__rbNames.GetSelection()))
			self.__toggleNames(None)
		elif keycode == ord('A'):
			self.__toggleAddPlanets(None)
		
		evt.Skip()
	
	def __onMouseClick(self, evt):
		self.__w.panel.SetFocusIgnoringChildren()
		evt.Skip()
		return
		
	
	def __saveSelectedBody(self, evt):
		baseErrorMessage = "Valor invalido em "
		
		try:
			errorAt = "Posicao X."
			posx = double(self.__infoPosXarea.GetValue())
			errorAt = "Posicao Z."
			posz = double(self.__infoPosZarea.GetValue())
			errorAt = "Translacao X."
			velx = double(self.__infoTransVXarea.GetValue())
			errorAt = "Translacao Z."
			velz = double(self.__infoTransVZarea.GetValue())
			errorAt = "Massa."
			mass = double(self.__infoMassArea.GetValue())
			errorAt = "Raio."
			radius = double(self.__infoRadiusArea.GetValue())
			errorAt = "Nome."
			name = self.__infoNameArea.GetValue()
			errorAt = "Rotacao."
			rots = double(self.__infoRotSarea.GetValue())
			self.__planetColorCombo.GetCurrentSelection()
		except ValueError:
			self.__errorMessage(baseErrorMessage+errorAt)
			return
		
		if type(self.__simulation.getSelectedBody()[0] is Planet):
			body = Planet(radius = 0, transVelocity = vector(velx, 0, velz))
		elif type(self.__simulation.getSelectedBody()[0] is Star):
			body = Star(radius = 0)
			
		body.setPosition(vector(posx, 0, posz))
		body.setMass(mass)
		body.setRadius(radius)
		body.setName(name)
		body.setRotSpeed(rots)
		
		self.__saveBodyBtn.Hide()
		self.__editBodyBtn.Show()
		
		self.__infoNameArea.Hide()
		self.__infoNameLabel.Hide()
		
		self.__setInfoAreaEditable(False)
		
		self.__simulation.editSelectedBody(body)
	
	def __getColorChoice(self, opt):
		return self.__COLORS_MAP.getItemAt(opt)
	
	def __removeSelectedBody(self, evt):
		self.__simulation.removeSelectedBody()
		self.__saveBodyBtn.Hide()
		self.__editBodyBtn.Show()
		self.__removeBodyBtn.Enable()
		self.__setInfoAreaEditable(False)
		self.__clearInfoAreaFields()
	
	
	def __setAnimationRate(self, evt):
		self.__aRate = self.__rateSlider.GetValue()
	
	
	def __toggleTrail(self, evt):
		choice = self.__rbOrbits.GetSelection()
		if choice == 0:
			self.__simulation.showTrails(True)
		else:
			self.__simulation.showTrails(False)
	
	
	def __toggleNames(self, evt):
		choice = self.__rbNames.GetSelection()
		if choice == 0:
			self.__simulation.showNames(True)
		else:
			self.__simulation.showNames(False)
		
		
	def __toggleAddPlanets(self, evt):
		self.__addingPlanets = not (self.__addingPlanets)
		bool = self.__addingPlanets
		
		if bool:
			self.__posXaddArea.Show()
			self.__posZaddArea.Show()
			self.__massAddArea.Show()
			self.__radiusAddArea.Show()
			self.__transVXaddArea.Show()
			self.__transVZaddArea.Show()
			self.__rotSaddArea.Show()
			self.__nameAddArea.Show()
			
			self.__planetColorCombo.Show()
			
			self.__posXaddLabel.Show()
			self.__posZaddLabel.Show()
			self.__massAddLabel.Show()
			self.__radiusAddLabel.Show()
			self.__transVXaddLabel.Show()
			self.__transVZaddLabel.Show()
			self.__rotSaddLabel.Show()
			self.__colorComboLabel.Show()
			self.__nameAddLabel.Show()
			
			self.__planetAddBtn.Show()
			
		else:
			self.__posXaddArea.Hide()
			self.__posZaddArea.Hide()
			self.__massAddArea.Hide()
			self.__radiusAddArea.Hide()
			self.__transVXaddArea.Hide()
			self.__transVZaddArea.Hide()
			self.__rotSaddArea.Hide()
			self.__nameAddArea.Hide()
			
			self.__planetColorCombo.Hide()
			
			self.__posXaddLabel.Hide()
			self.__posZaddLabel.Hide()
			self.__massAddLabel.Hide()
			self.__radiusAddLabel.Hide()
			self.__transVXaddLabel.Hide()
			self.__transVZaddLabel.Hide()
			self.__rotSaddLabel.Hide()
			self.__colorComboLabel.Hide()
			self.__nameAddLabel.Hide()
			
			self.__planetAddBtn.Hide()

			
	def __errorMessage(self, message):
		wx.MessageBox(message, 'Erro de preenchimento', 
            wx.OK | wx.ICON_INFORMATION)
	
	
	def __addPlanet(self, evt):
		newPlanet = Planet(radius = 0)
		baseErrorMessage = "Valor invalido em "
		
		try:
			errorAt = "Posicao X."
			posx = double(self.__posXaddArea.GetValue())
			errorAt = "Posicao Z."
			posz = double(self.__posZaddArea.GetValue())
			errorAt = "Translacao X."
			velx = double(self.__transVXaddArea.GetValue())
			errorAt = "Translacao Z."
			velz = double(self.__transVZaddArea.GetValue())
			errorAt = "Massa."
			mass = double(self.__massAddArea.GetValue())
			errorAt = "Raio."
			radius = double(self.__radiusAddArea.GetValue())
			errorAt = "Nome."
			name = self.__nameAddArea.GetValue()
			errorAt = "Rotacao."
			rots = double(self.__rotSaddArea.GetValue())
			color = self.__getColorChoice(self.__planetColorCombo.GetCurrentSelection())
		except ValueError:
			self.__errorMessage(baseErrorMessage+errorAt)
			del newPlanet
			return
			
		newPlanet.setPosition(vector(posx, 0, posz))
		newPlanet.setTransVelocity(vector(velx, 0, velz))
		newPlanet.setMass(mass)
		newPlanet.setRadius(radius)
		newPlanet.setName(name)
		newPlanet.setRotSpeed(rots)	
		newPlanet.setColor(color)
		
		self.__simulation.addPlanet(newPlanet)
	
	
	def getAnimationRate(self):
		return self.__aRate
	
	
	def getColorChoice(self):
		return self.__planetColorCombo.GetCurrentSelection()
	
	
	def __pauseSimulation(self, evt):
		paused = self.__simulation.isPaused()
		if paused:
			self.__simulation.pauseSimulation(False)
			self.__pauseBtn.SetLabel("Pausar")
		else:
			self.__simulation.pauseSimulation(True)
			self.__pauseBtn.SetLabel("Rodar")
	
	
	def __setBodyInfo(self, body):
		self.__infoMassArea.SetValue(str(body.getMass()))
		self.__infoRadiusArea.SetValue(str(body.getRadius()))
		self.__infoPosXarea.SetValue(str(body.getPosition().x))
		self.__infoPosZarea.SetValue(str(body.getPosition().z))
		self.__infoRotSarea.SetValue(str(body.getRotSpeed()))
		
		if type(body) is Planet:
			self.__infoTransVXarea.SetValue(str(body.getTransVelocity().x))
			self.__infoTransVZarea.SetValue(str(body.getTransVelocity().z))
		else:
			self.__infoTransVXarea.SetValue(str(0.0))
			self.__infoTransVZarea.SetValue(str(0.0))
		
	
	
	def runSimulation(self):
		self.__simulation.run()
		body, index = self.__simulation.getSelectedBody()
		
		if self.__simulation.mouseClicked():
			self.__infoBox.SetLabel("Informacoes de:  "+body.getName())
			self.__setBodyInfo(body)
			if not(type(body) is Star):
				self.__removeBodyBtn.Enable()
			else:
				self.__removeBodyBtn.Disable()
		
		elif body != None and not (self.__simulation.isPaused()):
			self.__setBodyInfo(body)
